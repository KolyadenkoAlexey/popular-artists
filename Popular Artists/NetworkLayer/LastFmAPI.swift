//
//  LastFmAPI.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import UIKit

typealias QueryResult = (_ result: Data?, _ error: Error?) -> Void
typealias ImageQueryResult = (_ image: UIImage?, _ error: Error?) -> Void


class LastFMDataSource: ApplicationDataSourceNetworkLayerProtocol, ModelMapperProtocol {
    
    // MARK: - Props
    var APIToken: String = "e81f61890b7ff8633ca024d0faa449e7"
    var baseURL: String = "https://ws.audioscrobbler.com/2.0"
    
    let defaultSession = URLSession(configuration: .default)
    var artistsDataTask: URLSessionDataTask?
    var albumsDataTask: URLSessionDataTask?
    var songsDataTask: URLSessionDataTask?
    
    // MARK: - Tasks
    func getPopularArtists(forCountry country: String, completion: QueryResult?) {
        artistsDataTask?.cancel()
        artistsDataTask = dataTask(withQuery: "method=geo.gettopartists&country=\(country)&api_key=\(APIToken)&format=json", completion: completion)
        artistsDataTask?.resume()
    }
    
    func getTopAlbums(forArtistIndentifier identifier: String, completion: QueryResult?) {
        albumsDataTask?.cancel()
        albumsDataTask = dataTask(withQuery: "method=artist.gettopalbums&mbid=\(identifier)&api_key=\(APIToken)&format=json", completion: completion)
        albumsDataTask?.resume()
    }
    
    func getSongs(forAlbum albumIdentifier: String, completion: QueryResult?) {
        songsDataTask?.cancel()
        songsDataTask = dataTask(withQuery: "method=album.getinfo&api_key=\(APIToken)&mbid=\(albumIdentifier)&format=json", completion: completion)
        songsDataTask?.resume()
    }
    
    func dataTask(withQuery query: String?, completion: QueryResult?) -> URLSessionDataTask? {
        if var urlComponents = URLComponents(string: baseURL) {
            urlComponents.query = query
            
            guard let url = urlComponents.url else { return nil }
            
            let task = defaultSession.dataTask(with: url) { data, response, error in
                if let error = error {
                    completion?(nil, error)
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    DispatchQueue.main.async {
                        completion?(data, error)
                    }
                }
            }
            
            return task
        }
        return nil
    }
    
    
    // MARK: - Images and cache
    func downloadImage(fromURL urlString: String, completion: ImageQueryResult?) {
        self.imageDownloadTasks[urlString]?.cancel()
            if let url = URL(string: urlString) {
                let task = defaultSession.dataTask(with: url) { (data, response, error) in
                    guard let data = data else {
                        completion?(nil, error)
                        return
                    }
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        completion?(image, error)
                        self.imageDownloadTasks.removeValue(forKey: urlString)
                        self.images[urlString] = image
                    }
                }
                imageDownloadTasks[urlString] = task
                task.resume()
            }   else {
                completion?(nil, nil)
        }
    }
    
    func fetchImageFromCacheOrDownload(fromURL url: String, completion: ImageQueryResult?) {
        if let cachedImage = self.images[url] {
            completion?(cachedImage, nil)
            return
        }
        
        self.downloadImage(fromURL: url, completion: completion)
    }
    

    // MARK: - Parsing
    func parseArtists(fromResponse response: Data) -> [ArtistModel]? {
        let responseJSON = try? JSONSerialization.jsonObject(with: response, options: []) as? [String: [String: Any]]
        let topArtists = responseJSON??["topartists"]
        let artistsDict = topArtists?["artist"] as? [[String: Any]]
        let artists = artistsDict?.map({ (dict) -> Artist in
            let artist = Artist()
            artist.listenersCount = Int((dict["listeners"] as? String) ?? "0")
            artist.name = dict["name"] as? String
            artist.identifier = dict["mbid"] as? String
            let images = dict["image"] as? [[String: String]]
            
            images?.forEach({ (dict) in
                if let size = dict["size"] {
                    if size == "large" {
                        artist.imageURLString = dict["#text"]
                    }
                }
            })
            return artist
        })
        return artists
    }
    
    func parseAlbums(fromResponse response: Data) -> [AlbumModel]? {
        let responseJSON = try? JSONSerialization.jsonObject(with: response, options: []) as? [String: [String: Any]]
        let topAlbums = responseJSON??["topalbums"]
        let albumsDict = topAlbums?["album"] as? [[String: Any]]
        let albums = albumsDict?.compactMap({ (dict) -> Album? in
            let album = Album()
            album.title = dict["name"] as? String
            album.identifier = dict["mbid"] as? String
            if album.identifier == nil {
                return nil
            }
            let images = dict["image"] as? [[String: String]]
            
            images?.forEach({ (dict) in
                if let size = dict["size"] {
                    if size == "large" {
                        album.artworkURLString = dict["#text"]
                    }   else if size == "extralarge" {
                        album.largeArtworkURLString = dict["#text"]
                    }
                }
            })
            return album
        })
        return albums
    }
    
    func parseSongs(fromResponse response: Data) -> [SongModel]? {
        let responseJSON = try? JSONSerialization.jsonObject(with: response, options: []) as? [String: [String: Any]]
        let albumsDict = responseJSON??["album"]
        let songs = albumsDict?["tracks"] as? [String: Any]
        let songsArray = songs?["track"] as? [[String: Any]]
        return songsArray?.compactMap({ (dict) -> SongModel in
            let song = Song()
            song.duration = Double((dict["duration"] as? String) ?? "0")
            song.title = dict["name"] as? String
            return song
        })
    }
    
    var imageDownloadTasks: [String: URLSessionDataTask] = [:]
    var images: [String: UIImage] = [:]
}

