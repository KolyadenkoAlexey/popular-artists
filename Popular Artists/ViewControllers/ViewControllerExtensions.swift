//
//  ViewControllerExtensions.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

extension UIViewController {
    static func initFromStoryboard(storyboardName name: String? = nil, viewControllerIdentifier identifier: String? = nil) -> UIViewController? {
        let className = String(describing: self)
        let storyboard = UIStoryboard.init(name: className, bundle: nil)
        let vc = identifier == nil ? storyboard.instantiateInitialViewController() : storyboard.instantiateViewController(withIdentifier: identifier!)
        return vc
    }
}

extension UIViewController {
    func add(_ child: UIViewController) {
        addChildViewController(child)
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        
        willMove(toParentViewController: nil)
        removeFromParentViewController()
        view.removeFromSuperview()
    }
}

extension UIViewController {
    func presentLoadingViewController() -> LoadingViewController {
        let loadingViewController = LoadingViewController()
        add(loadingViewController)
        return loadingViewController
    }
    
    func presentErrorViewController(withError error: Error?, tapAction: (() -> Void)?) -> ErrorViewController {
        let errorViewController = ErrorViewController()
        
        let errorMessage = error?.localizedDescription ?? "Error Occured. Tap to retry"
        self.add(errorViewController)
        errorViewController.tapAction = {
            errorViewController.remove()
            tapAction?()
        }
        errorViewController.errorMessage = errorMessage
        return errorViewController
    }
}
