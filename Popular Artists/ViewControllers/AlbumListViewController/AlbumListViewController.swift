//
//  AlbumListViewController.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class AlbumListViewController: UIViewController {
    
    // MARK: - Public Static
    class func initWith(albumsDataSource dataSource: AlbumListDataSource?, artist: ArtistModel) -> AlbumListViewController? {
        let vc = AlbumListViewController.initFromStoryboard() as? AlbumListViewController
        vc?.dataSource = dataSource
        vc?.artist = artist
        return vc
    }
    
    // MARK: - Public Properties
    var artist: ArtistModel!
    var dataSource: AlbumListDataSource!
    var delegate: AlbumListDelegate?
    
    // MARK: - Private Props
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    var loadingViewController: LoadingViewController?
    var errorViewController: ErrorViewController?
    
    
    private var albums: [AlbumModel]? = nil {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        populateAlbums()
    }
    
    // MARK: DataSource Convenience
    func populateAlbums() {
        self.loadingViewController?.remove()
        self.errorViewController?.remove()
        
        self.loadingViewController = presentLoadingViewController()
        dataSource.fetchAlbums(forArtist: self.artist) { (albums, error) in
            self.loadingViewController?.remove()
            guard let albums = albums else {
                self.errorViewController = self.presentErrorViewController(withError: error, tapAction: { self.populateAlbums() })
                self.albums = nil
                return
            }
            self.albums = albums
        }
    }
}

extension AlbumListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.albums?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionViewCell", for: indexPath)
        if let model = self.albums?[indexPath.row], let cell = cell as? AlbumCollectionViewCell {
            self.dataSource.fetchArtwork(forAlbum: model) { (image, error) in
                DispatchQueue.main.async {
                    cell.artworkImageView?.image = image
                }
            }
            cell.titleLabel?.text = model.title
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let model = self.albums?[indexPath.row] {
            self.delegate?.didSelect(album: model)
        }
    }
}

extension AlbumListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = view.frame.size.width
        return CGSize(width: viewWidth / 2, height: (viewWidth / 2) * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class AlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var artworkImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.artworkImageView?.image = nil
        self.titleLabel?.text = nil
    }
}


