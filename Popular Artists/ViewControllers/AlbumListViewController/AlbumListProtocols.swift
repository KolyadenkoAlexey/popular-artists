//
//  AlbumsListProtocols.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation

// MARK: - ViewController related
typealias AlbumListFetchCompletionHandler = (_ albums: [AlbumModel]?, _ error: Error?) -> Void

protocol AlbumListDataSource {
    func fetchAlbums(forArtist artist: ArtistModel, completionHandler: AlbumListFetchCompletionHandler?)
    func fetchArtwork(forAlbum album: AlbumModel, completionHandler: ImageFetchCompletionHandler?)
}

protocol AlbumListDelegate {
    func didSelect(album: AlbumModel)
}

// MARK: - Model
protocol AlbumModel: Identifiable {
    var title: String! { get }
}
