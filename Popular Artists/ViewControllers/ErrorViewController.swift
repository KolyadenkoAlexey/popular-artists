//
//  ErrorViewController.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {
    private lazy var errorLabel = UILabel()
    
    var errorMessage: String? {
        didSet {
            errorLabel.text = errorMessage
            errorLabel.sizeToFit()
        }
    }
    var tapAction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(errorLabel)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ErrorViewController.viewTapped)))
        
        NSLayoutConstraint.activate([
            errorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            errorLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 24)
            ])
    }
    
    @objc func viewTapped() {
        self.tapAction?()
    }
}
