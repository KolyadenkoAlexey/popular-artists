//
//  AlbumSongsViewController.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class AlbumSongsViewController: UIViewController {
    
    // MARK: - Public Static
    class func initWith(dataSource: AlbumDetailsDataSource?, album: AlbumModel) -> AlbumSongsViewController? {
        let vc = AlbumSongsViewController.initFromStoryboard() as? AlbumSongsViewController
        vc?.dataSource = dataSource
        vc?.album = album
        return vc
    }
    
    // MARK: - Public Properties
    var dataSource: AlbumDetailsDataSource!
    
    var album: AlbumModel!
    
    // MARK: - Private Properties
    var songs: [SongModel]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    var loadingViewController: LoadingViewController?
    var errorViewController: ErrorViewController?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        populateSongs()
    }
    
    // MARK: - DataSource convenience
    func populateSongs() {
        self.loadingViewController?.remove()
        self.errorViewController?.remove()
        guard let album = self.album else { return }
        self.loadingViewController = presentLoadingViewController()
        dataSource.fetchSongs(fromAlbum: album) { (songs, error) in
            self.loadingViewController?.remove()
            guard let songs = songs else {
                self.errorViewController = self.presentErrorViewController(withError: error, tapAction: { self.populateSongs() })
                self.songs = []
                return
            }
            self.songs = songs
        }
    }
}

extension AlbumSongsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArtworkTableViewCell", for: indexPath)
            if let cell = cell as? ArtworkTableViewCell {
                self.dataSource.fetchLargeArtwork(forAlbum: self.album) { (image, error) in
                    DispatchQueue.main.async {
                        cell.artworkImageView?.image = image
                    }
                }
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SongTableViewCell", for: indexPath)
            if let cell = cell as? SongTableViewCell, let model = self.songs?[indexPath.row] {
                let minutes = Int(floor((model.duration / 60)))
                let seconds = Int(Int(model.duration) - minutes * 60)
                var secondsString: String! = nil
                if seconds > 9 {
                    secondsString = "\(seconds)"
                }   else {
                    secondsString = "0\(seconds)"
                }
                cell.songTitleLabel.text = "\(indexPath.row + 1). \(model.title!) (\(minutes):\(secondsString!))"
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return self.songs?.count ?? 0
        }
    }
}

class ArtworkTableViewCell: UITableViewCell {
    @IBOutlet weak var artworkImageView: UIImageView?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.artworkImageView?.image = nil
    }
}

class SongTableViewCell: UITableViewCell {
    @IBOutlet weak var songTitleLabel: UILabel!
}


