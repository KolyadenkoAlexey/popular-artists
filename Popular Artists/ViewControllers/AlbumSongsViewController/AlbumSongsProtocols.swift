//
//  AlbumSongsProtocols.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/4/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation

typealias SongsFetchCompletionHandler = (_ songs: [SongModel]?, _ error: Error?) -> Void

protocol SongModel {
    var title: String! { get }
    var duration: TimeInterval! { get }
}

protocol AlbumDetailsDataSource {
    func fetchSongs(fromAlbum album: AlbumModel, completionHandler: SongsFetchCompletionHandler?)
    func fetchLargeArtwork(forAlbum album: AlbumModel, completionHandler: ImageFetchCompletionHandler?)
}
