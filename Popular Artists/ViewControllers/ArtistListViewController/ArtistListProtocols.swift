//
//  ArtistListProtocols.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ViewController related
typealias ArtistsFetchCompletionHandler = (_ artists: [ArtistModel]?, _ error: Error?) -> Void
typealias ImageFetchCompletionHandler = (_ image: UIImage?, _ error: Error?) -> Void

protocol ArtistListDataSource {
    func fetchTopArtists(forCountry country: String, completionHandler: ArtistsFetchCompletionHandler?)
    func fetchImage(forArtist artist: ArtistModel, completionHandler: ImageFetchCompletionHandler?)
}

protocol ArtistListDelegate {
    func didSelect(artist: ArtistModel)
}


// MARK: - Model
protocol ArtistModel: Identifiable {
    var name: String! { get }
    var listenersCount: Int! { get }
}

protocol Identifiable {
    var identifier: String! { get }
}


