//
//  ArtistListViewController.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class ArtistListViewController: UIViewController {
    
    // MARK: - Public Static
    class func initWith(artistsDataSource dataSource: ArtistListDataSource?) -> ArtistListViewController? {
        let vc = ArtistListViewController.initFromStoryboard() as? ArtistListViewController
        vc?.dataSource = dataSource
        return vc
    }
    
    // MARK: - Public Properties
    var dataSource: ArtistListDataSource!
    var delegate: ArtistListDelegate?
    
    // MARK: - Private Props
    private var artists: [ArtistModel]? = nil {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = 100
            tableView.tableFooterView = UIView()
        }
    }
    var loadingViewController: LoadingViewController?
    var errorViewController: ErrorViewController?
    
    // MARK: - Country
    private var availableCountries: [String] { return  ["Spain", "Ukraine", "Netherlands", "Israel", "Italy"] }
    private var selectedCountry: String? {
        didSet {
            self.countryButton.setTitle(selectedCountry, for: .normal)
            self.populateArtists()
        }
    }
    
    @IBOutlet weak var countryButton: UIButton!
    @IBAction func countryButtonPressed(_ sender: UIButton) {
        let sheet = UIAlertController(title: "Country", message: "Select country", preferredStyle: .actionSheet)
        availableCountries.forEach { (country) in
            let action = UIAlertAction(title: country, style: .default, handler: { (action) in
                self.selectedCountry = country
            })
            sheet.addAction(action)
        }
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(sheet, animated: true, completion: nil)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedCountry = availableCountries.first
    }
    
    // MARK: DataSource Convenience
    func populateArtists() {
        guard let selectedCountry = selectedCountry else { return }
        
        self.loadingViewController?.remove()
        self.errorViewController?.remove()

        self.loadingViewController = presentLoadingViewController()
        dataSource.fetchTopArtists(forCountry: selectedCountry) { (artists, error) in
            self.loadingViewController?.remove()
            guard let artists = artists else {
                self.errorViewController = self.presentErrorViewController(withError: error, tapAction: { self.populateArtists() })
                self.artists = nil
                return
            }
            self.artists = artists
        }
    }
}

extension ArtistListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.artists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell", for: indexPath) as? ArtistTableViewCell else {
            assertionFailure("tableView failed to dequeue ArtistTableViewCell")
            return UITableViewCell()
        }
        let model = self.artists![indexPath.row]

        self.dataSource.fetchImage(forArtist: model) { (image, error) in
            DispatchQueue.main.async {
                cell.artistImageView.image = image
            }
        }
        
        cell.nameLabel.text = model.name
        let listeners = model.listenersCount ?? 0
        cell.subtitleLabel.text = "(\(listeners) Listeners)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = self.artists?[indexPath.row] {
            delegate?.didSelect(artist: model)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}



class ArtistTableViewCell: UITableViewCell {
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        artistImageView.image = nil
        nameLabel.text = nil
        subtitleLabel.text = nil
    }
}
