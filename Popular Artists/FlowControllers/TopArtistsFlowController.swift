//
//  TopArtistsFlowController.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import UIKit

class TopArtistsFlowController {
    public struct Configuration {
        var artistsDataSource: ArtistListDataSource
        var albumsDataSource: AlbumListDataSource
        var albumDetailsDataSource: AlbumDetailsDataSource
    }
    
    var configuration: TopArtistsFlowController.Configuration
    
    init(configuration: TopArtistsFlowController.Configuration) {
        self.configuration = configuration
    }
    
    var navigationController: UINavigationController?
    
    func start(inWindow window: UIWindow) {
        let artistsVC = ArtistListViewController.initWith(artistsDataSource: self.configuration.artistsDataSource)!
        artistsVC.delegate = self
        self.navigationController = UINavigationController(rootViewController: artistsVC)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

extension TopArtistsFlowController: ArtistListDelegate {
    func didSelect(artist: ArtistModel) {
        let albumListViewController = AlbumListViewController.initWith(albumsDataSource: configuration.albumsDataSource, artist: artist)!
        albumListViewController.delegate = self
        self.navigationController?.pushViewController(albumListViewController, animated: true)
    }
}

extension TopArtistsFlowController: AlbumListDelegate {
    func didSelect(album: AlbumModel) {
        let albumSongsViewController = AlbumSongsViewController.initWith(dataSource: configuration.albumDetailsDataSource, album: album)!
        self.navigationController?.pushViewController(albumSongsViewController, animated: true)
    }
}
