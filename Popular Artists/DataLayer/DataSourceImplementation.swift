//
//  DataSourceImplementation.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/4/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation

// MARK: - Conecrete class
class ApplicationDataSource: ArtistListDataSource, AlbumListDataSource, AlbumDetailsDataSource {
    var networkLayer: ApplicationDataSourceNetworkLayerProtocol!
    var modelMapper: ModelMapperProtocol!
    
    init(networkLayer: ApplicationDataSourceNetworkLayerProtocol, modelMapper: ModelMapperProtocol) {
        self.networkLayer = networkLayer
        self.modelMapper = modelMapper
    }
    
    func fetchTopArtists(forCountry country: String, completionHandler: ArtistsFetchCompletionHandler?) {
        self.networkLayer.getPopularArtists(forCountry: country) { (response, error) in
            guard response != nil else {
                completionHandler?(nil, error)
                return
            }
            completionHandler?(self.modelMapper.parseArtists(fromResponse: response!)
                , error)
        }
    }
    
    func fetchImage(forArtist artist: ArtistModel, completionHandler: ImageFetchCompletionHandler?) {
        guard let artist = artist as? Artist, artist.imageURLString != nil else {
            completionHandler?(nil, nil)
            return
        }
        self.networkLayer.fetchImageFromCacheOrDownload(fromURL: artist.imageURLString!, completion: completionHandler)
    }
    
    func fetchAlbums(forArtist artist: ArtistModel, completionHandler: AlbumListFetchCompletionHandler?) {
        self.networkLayer.getTopAlbums(forArtistIndentifier: artist.identifier) { (data, error) in
            guard let data = data else {
                completionHandler?(nil, error)
                return
            }
            completionHandler?(self.modelMapper.parseAlbums(fromResponse: data), nil)
        }
    }
    
    func fetchArtwork(forAlbum album: AlbumModel, completionHandler: ImageFetchCompletionHandler?) {
        if let album = album as? Album, album.artworkURLString != nil {
            self.networkLayer.fetchImageFromCacheOrDownload(fromURL: album.artworkURLString!, completion: completionHandler)
        }   else {
            completionHandler?(nil, nil)
        }
    }
    
    func fetchLargeArtwork(forAlbum album: AlbumModel, completionHandler: ImageFetchCompletionHandler?) {
        if let album = album as? Album, album.artworkURLString != nil {
            self.networkLayer.fetchImageFromCacheOrDownload(fromURL: album.largeArtworkURLString!, completion: completionHandler)
        }   else {
            completionHandler?(nil, nil)
        }
    }
    
    func fetchSongs(fromAlbum album: AlbumModel, completionHandler: SongsFetchCompletionHandler?) {
        guard let identifier = album.identifier else {
            completionHandler?(nil, nil)
            return
        }
        self.networkLayer.getSongs(forAlbum: identifier) { (data, error) in
            guard let data = data else {
                completionHandler?(nil, error)
                return
            }
            
            let songs = self.modelMapper.parseSongs(fromResponse: data)
            completionHandler?(songs, nil)
        }
    }
}

// MARK: - Protocols
protocol ApplicationDataSourceNetworkLayerProtocol {
    func getPopularArtists(forCountry country: String, completion: QueryResult?)
    func getTopAlbums(forArtistIndentifier identifier: String, completion: QueryResult?)
    func getSongs(forAlbum albumIdentifier: String, completion: QueryResult?)
    func downloadImage(fromURL urlString: String, completion: ImageQueryResult?)
    func fetchImageFromCacheOrDownload(fromURL url: String, completion: ImageQueryResult?)
}

protocol ModelMapperProtocol {
    func parseArtists(fromResponse response: Data) -> [ArtistModel]?
    func parseAlbums(fromResponse response: Data) -> [AlbumModel]?
    func parseSongs(fromResponse response: Data) -> [SongModel]?
}
