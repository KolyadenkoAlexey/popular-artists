//
//  Models.swift
//  Popular Artists
//
//  Created by Alexey Kolyadenko on 5/3/18.
//  Copyright © 2018 Alexey Kolyadenko. All rights reserved.
//

import Foundation

class Artist: ArtistModel {
    var identifier: String!
    var name: String!
    var listenersCount: Int!
    var imageURLString: String?
}

class Album: AlbumModel {
    var identifier: String!
    var title: String!
    var artworkURLString: String!
    var largeArtworkURLString: String!
}

class Song: SongModel {
    var title: String!
    var duration: TimeInterval!
}
